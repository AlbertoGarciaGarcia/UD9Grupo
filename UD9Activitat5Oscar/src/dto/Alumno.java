package dto;

public class Alumno extends Persona {
    //Hago el atributo de alumno
    private int nota;
     
    //Hago el constructor
    public Alumno(){
        super();
         
        nota=MetodoAleatorio.generaNumeroAleatorio(0,10);
         
        super.setEdad(MetodoAleatorio.generaNumeroAleatorio(12,16));
         
    }
	//Hago el metodo para dar y cambiar la nota
    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
	//Para comprobar si el alumno esta
    public void disponibilidad() {
        
        int prob=MetodoAleatorio.generaNumeroAleatorio(0, 100);
         
        if(prob<50){
            super.setAsistencia(false);
        }else{
            super.setAsistencia(true);
        }
         
    }
	@Override
	public String toString() {
		return "nombre: " + super.getNombre() + " ,sexo: " + super.getSexo() + " ,nota: " + nota;
	}
    

}
