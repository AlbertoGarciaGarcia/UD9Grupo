package model;

import util.Entregable;

public class serie implements Entregable{

		// Constantes
		protected final static int temporadas = 3;
		
		protected final static boolean entrega = false;
		
		// Atributos
		protected String titulo;
		
		protected int numTemporadas;
		
		protected boolean entregado;
		
		protected String genero;
		
		protected String creador;

		// Constructores
		public serie() {
			this.titulo = "";
			this.numTemporadas = temporadas;
			this.entregado = entrega;
			this.genero = "";
			this.creador = "";
		}

		public serie(String titulo, String creador) {
			this.titulo = titulo;
			this.creador = creador;
			this.numTemporadas = temporadas;
			this.entregado = entrega;
			this.genero = "";
		}

		public serie(String titulo, int numTemporadas, String genero, String creador) {
			this.titulo = titulo;
			this.numTemporadas = numTemporadas;
			this.genero = genero;
			this.creador = creador;
			this.entregado = entrega;
		}

		// toString
		@Override
		public String toString() {
			return "Serie [Titulo de la serie:" + titulo + ", el n�mero de temporadas que tiene son:" + numTemporadas + ", Ha sido entregada?" + entregado + ", el g�nero de la s�rie es:"
					+ genero + ", y el creador es:" + creador + "]";
		}

		// Getters y Setters
		public String getTitulo() {
			return titulo;
		}

		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}

		public int getNumTemporadas() {
			return numTemporadas;
		}

		public void setNumTemporadas(int numTemporadas) {
			this.numTemporadas = numTemporadas;
		}

		public String getGenero() {
			return genero;
		}

		public void setGenero(String genero) {
			this.genero = genero;
		}

		public String getCreador() {
			return creador;
		}

		public void setCreador(String creador) {
			this.creador = creador;
		}

		// M�todos implementados
		@Override
		public void entregar() {
			// Hago que el objeto pasado su entregado pase a true
			entregado = true;
			
		}

		@Override
		public void devolver() {
			// Hago que el objeto pasado su entregado pase a false
			entregado = false;
			
		}

		@Override
		public boolean isEntregado() {
			// Compruebo el entregado del objeto
			if(entregado == true) {
				return true;
			}
			else {
				return false;
			}
			
			
		}

		// Creo el compareTo
		@Override
		public String compareTo(Object objeto) {
			// Defino la cantidad de numero de temporadas a peque�o
			String cantNumTemporadas = "Peque�o";
			
			// Creo el objetoSerie el cual ser� igual al objeto pasado por par�metro casteado a serie
			serie objetoSerie = (serie) objeto;
			
			// Comparamos el n�mero de temporadas y definimos la cantidad de temporadas dependiendo al resultado
			if(numTemporadas > objetoSerie.getNumTemporadas()) {
				cantNumTemporadas = "Grande";
			}
			else if(numTemporadas == objetoSerie.getNumTemporadas()) {
				cantNumTemporadas = "Igual";
			}
			
			return cantNumTemporadas;
		}
		
		
}

