package model;

public class cine {

	// Constantes creadas para crear la matriz
	final static int numFilas = 8;
	
	final static int numColumnas = 9;
	
	final static boolean sala[][] = new boolean[numFilas][numColumnas];
	
	
	// Creo los atributos
	private double precioEntrada;
	
	private pelicula pelicula;
	
	// Constructor por defecto
	public cine() {
		this.precioEntrada = 0;
		this.pelicula = null;
		iniciarSalaVacia();
	}

	// Constructor con datos por par�metro
	public cine(double precioEntrada, model.pelicula pelicula) {
		this.precioEntrada = precioEntrada;
		this.pelicula = pelicula;
		iniciarSalaVacia();
	}
	
	// Creamos un m�todo para inicializar la sala en false todo
	public void iniciarSalaVacia() {
		
		for (int i = 0; i < sala.length; i++) {
			for (int j = 0; j < sala[0].length; j++) {
				sala[i][j] = false;
			}
		}
	}
	
	// Creamos un m�todo para comprobar si el cine est� lleno
	public boolean cineLleno() {
		
		boolean cineLleno = true;
		
		for (int i = 0; i < sala.length; i++) {
			for (int j = 0; j < sala[0].length; j++) {
				
				if (sala[i][j] == false) {
					cineLleno = false;
				}
			}
		}
		
		return cineLleno;
	}
	
	// Creamos un m�todo para asignar un asiento
	public void darAsiento() {
		
		boolean asignar = false;
		
		for (int i = 0; i < sala.length; i++) {
			for (int j = 0; j < sala[0].length; j++) {
				if (sala[i][j] == false) {
					if(asignar == false) {
						sala[i][j] = true;
						asignar = true;
					}
				}
			}
		}
	}

	// Getters y setters
	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	public pelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(pelicula pelicula) {
		this.pelicula = pelicula;
	}

	public static int getNumfilas() {
		return numFilas;
	}

	public static int getNumcolumnas() {
		return numColumnas;
	}	
	
	
}
