package dto;

public class Raices {
    //Creo los atributos
    private double a;
    private double b;
    private double c;
     
    //El constructor
    public Raices(double a, double b, double c){
        this.a=a;
        this.b=b;
        this.c=c;
    }
       
   //El que hace una raiz con una soluci�n
    private void obtenerRaiz(){
         
        double x=(-b)/(2*a);
         
        System.out.println("Unica solucion");
        System.out.println(x);
         
    }
    //Hace las raizes con mas de una soluci�n
    private void obtenerRaices(){
         
        double x1=(-b+Math.sqrt(getDiscriminante()))/(2*a);
        double x2=(-b-Math.sqrt(getDiscriminante()))/(2*a);
         
        System.out.println("Solucion X1");
        System.out.println(x1);
        System.out.println("Solucion X2");
        System.out.println(x2);
    }
     
    //Devuelvo un boolean indicando si tiene dos soluciones
    private boolean tieneRaices(){
        return getDiscriminante()>0;
    }
    
    //Devuelvo el valor discriminante
    private double getDiscriminante(){
        return Math.pow(b, 2)-(4*a*c);
    }
     
   //Devuelvo un boolean indicando si tiene una solucion
    private boolean tieneRaiz(){
        return getDiscriminante()==0;
    }
     
   // mostrara por consola las posibles soluciones que tiene nuestra ecuaci�n
    public void operaciones(){
         
        if(tieneRaices()){
            obtenerRaices();
        }else if(tieneRaiz()){
            obtenerRaiz();
        }else{
            System.out.println("No tiene posibles soluciones");
        }
         
    }
}
