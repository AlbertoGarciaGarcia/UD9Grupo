package model;

public class espectador {

	// Atributos espectador
	private String nombre;
	
	private int edad;
	
	private double dinero;

	// Constructores
	public espectador() {
		this.nombre = "";
		this.edad = 0;
		this.dinero = 0;
	}

	public espectador(String nombre, int edad, double dinero) {
		this.nombre = nombre;
		this.edad = edad;
		this.dinero = dinero;
	}

	// Getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getDinero() {
		return dinero;
	}

	public void setDinero(double dinero) {
		this.dinero = dinero;
	}

	// Creo el toString
	@Override
	public String toString() {
		return "espectador [nombre=" + nombre + ", edad=" + edad + ", dinero=" + dinero + "]";
	}
	
	
	
	
}
