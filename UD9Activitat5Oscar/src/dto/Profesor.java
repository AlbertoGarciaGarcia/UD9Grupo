package dto;

public class Profesor extends Persona{

    //Creo el atributo de professor
    private String materia;
    
    //Hago el constructor
    public Profesor(){
       super(); //Llama al constructor padre
        
       super.setEdad(MetodoAleatorio.generaNumeroAleatorio(22,60)); //llama al metodo padre
        
       materia=Constantes.MATERIA[MetodoAleatorio.generaNumeroAleatorio(0,2)];
    }
    //Los metodos
    //El get y set de materia
    public String getMateria() {
        return materia;
    }
 
    public void setMateria(String materia) {
        this.materia = materia;
    }
    //El de comprobar si esta disponible o no
    public void disponibilidad() {
        
        int prob=MetodoAleatorio.generaNumeroAleatorio(0, 100);
         
        if(prob<20){
            super.setAsistencia(false);
        }else{
            super.setAsistencia(true);
        }
         
    }
    

}
