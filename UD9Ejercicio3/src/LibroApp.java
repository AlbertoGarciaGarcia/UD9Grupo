import dto.Libro;

public class LibroApp {

	public static void main(String[] args) {
			Libro libro1=new Libro(54164512, "Quijote", "Cervantes", 300); // Creamos el primer objeto
	         
			Libro libro2=new Libro(45648645, "La Odisea", "Homero", 650); // Ahora el siguiente

			// Mostramos el libro, es decir, sus caracteristicas
			System.out.println(libro1.toString());
	        System.out.println(libro2.toString());
			
	        // Y llamamos al metodo get de las paginas y al metodo get del titulo para comparar los numeros de paginas e indicar el libro que tiene mas 
	        
	        if(libro1.getNumPaginas()>libro2.getNumPaginas()){
	            System.out.println(libro1.getTitulo()+" tiene m�s p�ginas");
	        }else{
	            System.out.println(libro2.getTitulo()+" tiene m�s p�ginas");
	        }
	         
			
			
			
			
			
			
			
			
			
	}

}
