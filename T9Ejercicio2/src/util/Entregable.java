package util;

public interface Entregable {

	// M�todos a implementar
	public void entregar();
	
	public void devolver();
	
	public boolean isEntregado();
	
	public String compareTo(Object objeto);
}
