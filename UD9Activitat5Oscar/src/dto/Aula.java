package dto;

public class Aula {
	//Creo los atributos
    private Profesor profesor;
    private Alumno[] alumnos;
    private String materia;
     
    //Creo la constante
    private final int MAX_ALUMNOS=20;
     
    //El constructor de aula
    public Aula(){
        
        profesor=new Profesor();
        alumnos= new Alumno[MAX_ALUMNOS];
        creaAlumnos();
        materia=Constantes.MATERIA[MetodoAleatorio.generaNumeroAleatorio(0,2)];
         
    }
     
    //Los metodos
    //El de crear un alumno todo aleatorio
    private void creaAlumnos(){
         
        for(int i=0;i<alumnos.length;i++){
            alumnos[i]=new Alumno();
        }
         
    }
    //Comprovar cuantos alumnos hay presentes
    private boolean asistenciaAlumnos(){
         
        int cuentaAsistencias=0;
         
        //contamos las asistencias
        for(int i=0;i<alumnos.length;i++){
             
            if(alumnos[i].isAsistencia()){
                cuentaAsistencias++;
            }
             
        }
        
         
        return cuentaAsistencias>=((int)(alumnos.length/2));
         
    }
     
    //Miro si puedo dar clase
    public boolean darClase(){
         
        //Las condiciones para que se pueda dar clase
        if(!profesor.isAsistencia()){
            System.out.println("El profesor no esta, no se puede dar clase");
            return false;
        }else if(!profesor.getMateria().equals(materia)){
            System.out.println("La materia del profesor y del aula no es la misma, no se puede dar clase");
            return false;
        }else if (!asistenciaAlumnos()){
            System.out.println("La asistencia no es suficiente, no se puede dar clase");
            return false;
        }
         
        System.out.println("Se puede dar clase");
        System.out.println("Alumnos:");
        return true;
         
    }
     
    //Miro las notas de los alumnos y pongo en un total cuantos chicos y chicas est�n aprovados
    public void notas(){
         
        int chicosApro=0;
        int chicasApro=0;
         
         for(int i=0;i<alumnos.length;i++){
            
           //Comprobamos si el alumno esta aprobado
           if(alumnos[i].getNota()>=5){
               //Segun el sexo, aumentara uno o otro
               if(alumnos[i].getSexo()=='H'){
                   chicosApro++;
               }else{
                   chicasApro++;
               }
                
               System.out.println(alumnos[i].toString());  
           }
             
        }
        //ense�o cuantos en total hay aprovados
        System.out.println("Hay "+chicosApro+" chicos y "+chicasApro+" chicas aprobados/as");
         
    }
}
