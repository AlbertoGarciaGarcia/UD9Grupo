import model.serie;
import model.videojuego;

public class SerieApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo los dos arrays
		serie arraySeries[] = new serie[5];
		videojuego arrayVideojuegos[] = new videojuego[5];
		
		// A�ado los objetos al array de series
		arraySeries[0] = new serie("Game Of Thrones", "David Benioff");
		arraySeries[1] = new serie();
		arraySeries[2] = new serie("Sons Of Anarchy", "Kurt Sutter");
		arraySeries[3] = new serie("Prison Break", 7, "Drama", "Paul Scheuring");
		arraySeries[4] = new serie();
		
		// A�ado los objetos al array de videojuegos
		arrayVideojuegos[0] = new videojuego();
		arrayVideojuegos[1] = new videojuego("Pok�mon Espada", 100, "Aventuras", "Game Freak");
		arrayVideojuegos[2] = new videojuego("The Witcher 3", 300, "RPG/Acci�n", "CD Projekt Red");
		arrayVideojuegos[3] = new videojuego("Pok�mon Mundo Misterioso", 150);
		arrayVideojuegos[4] = new videojuego("For The King", 275);
	
		// Entrego las series y los juegos
		arraySeries[0].entregar();
		arraySeries[2].entregar();
		arraySeries[3].entregar();
		arrayVideojuegos[1].entregar();
		arrayVideojuegos[2].entregar();
		
		// Creo los contadores
		int contSeriesEntregadas = 0;
		int contVideojuegosEntregados = 0;
		
		// Hago el for para recorrer los arrays y mirar que articulos 
		// estan entregados
		for (int i = 0; i < arrayVideojuegos.length; i++) {
			if(arraySeries[i].isEntregado() == true) {
				// Sumo al contador
				contSeriesEntregadas++;
				// Devuelvo el articulo
				arraySeries[i].devolver();
			}
			if(arrayVideojuegos[i].isEntregado() == true) {
				// Sumo al contador
				contVideojuegosEntregados++;
				// Devuelvo el articulo
				arraySeries[i].devolver();
			}
		}
		
		// Asigno a los objetos mayores la primera posicion de los arrays
		serie serieMayorNumTemporadas = arraySeries[0];
		videojuego videojuegoMayorHorasEstimadas = arrayVideojuegos[0];
		
		// Recorro los dos arrays para recoger los mayores de los dos
		for (int i = 1; i < arrayVideojuegos.length; i++) {
			
			if (arraySeries[i].compareTo(serieMayorNumTemporadas) == "Grande") {
				// Convierto al objeto mayor a ese espacio del array
				serieMayorNumTemporadas = arraySeries[i];
			}
			
			if (arrayVideojuegos[i].compareTo(videojuegoMayorHorasEstimadas) == "Peque�o") {
				// Convierto al objeto mayor a ese espacio del array
				videojuegoMayorHorasEstimadas = arrayVideojuegos[i];
			}
		}
		
		// Muestro toda la informaci�n
		System.out.println("Hay " + contSeriesEntregadas + " series entregadas");
		System.out.println("Hay " + contVideojuegosEntregados + " videojuegos entregados");
		System.out.println(serieMayorNumTemporadas);
		System.out.println(videojuegoMayorHorasEstimadas);
	}

}
