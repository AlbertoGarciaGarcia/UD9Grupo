package dto;

public class Libro {
	
	// Creamos los atributos, no tienen ningun valor por defecto ya que el ennunciado del ejercicio no lo pide.
	private int ISBN;
    private String titulo;
    private String autor;
    private int numPaginas;
    
     // Hacemos un get y un set de todos los atributos para poder llamarlos adecuadamente con el constructor
    public int getISBN() {
        return ISBN;
    }
 
    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }
 
    public String getTitulo() {
        return titulo;
    }
 
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
 
    public String getAutor() {
        return autor;
    }
 
    public void setAutor(String autor) {
        this.autor = autor;
    }
 
    public int getNumPaginas() {
        return numPaginas;
    }
 
    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }
    // Sobreescribimos el toString para introducir el formato deseado en el enunnciado.
    @Override
    public String toString(){
        return "El libro "+titulo+" con ISBN "+ISBN+""	 + " creado por el autor "+autor + " tiene "+numPaginas+" p�ginas";
               
                
    } 
 
    // Creamos un constrcutor con todos los valores
    public Libro(int pISBN, String pTitulo, String pAutor, int pNumPaginas){
        
        ISBN=pISBN;
        titulo=pTitulo;
        autor=pAutor;
        numPaginas=pNumPaginas;
     
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
