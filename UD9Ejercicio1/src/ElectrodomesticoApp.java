import dto.Lavadora;
import dto.Electrodomestico;
import dto.Television;
public class ElectrodomesticoApp {

	public static void main(String[] args) {
		
		Electrodomestico arrayObjetos[] = new Electrodomestico[10]; // We create the array
		// We create 10 objects to add them to the array
		Lavadora lavadora1 = new Lavadora(130.25,150,'D',"Verde",35); 
		Lavadora lavadora2 = new Lavadora(130.23,130,'C',"Blanco",24);
		Lavadora lavadora3 = new Lavadora(189.50,120,'A',"Azul",36);
		Lavadora lavadora4 = new Lavadora(215.30,110,'D',"Negro",29);
		Lavadora lavadora5 = new Lavadora(190.25,150,'F',"Marron",35); 
		Television television1 = new Television(450,72,'B',"Lila",24,true);; 
		Television television2 = new Television(200,90,'F',"Blanco",29,false);
		Television television3 = new Television(200,90,'F',"Blanco",29,false);
		Television television4 = new Television(280,35,'D',"Gris",32,true);
		Electrodomestico electrodomestico1 = new Electrodomestico(200,50);
		// And we assign the objects to the positions of the array
		arrayObjetos[0] = lavadora1;	
		arrayObjetos[1] = lavadora2;		
		arrayObjetos[2] = lavadora3;
		arrayObjetos[3] = lavadora4;
		arrayObjetos[4] = lavadora5;
		arrayObjetos[5] = television1;
		arrayObjetos[6] = television2;
		arrayObjetos[7] = television3;
		arrayObjetos[8] = television4;
		arrayObjetos[9] = electrodomestico1;
		
		double totalLavadoras = 0; // We create the total variables to add all the values of the different types of objects.
		double totalTelevisiones = 0;
		double totalElectrodomesticos = 0;
		for (int i = 0; i < arrayObjetos.length; i++) { // We do as many loops as the number of positions inside  the array.
			if(arrayObjetos[i] instanceof Electrodomestico ) { // Here we use the instanceof method to define the type of object and compare it to 
				   // the object that is being given inside the position of the array
					totalElectrodomesticos = totalElectrodomesticos + arrayObjetos[i].precioFinal(); // All of the objects will equal "Electrodomestico", so each one will be added to it

}
			if(arrayObjetos[i] instanceof Lavadora ) { // If it equals Lavadora, we add it to Lavadora
				totalLavadoras = totalLavadoras + arrayObjetos[i].precioFinal();
			
			}
			
		 if(arrayObjetos[i] instanceof Television ) { // And if equals Television, we add it to Television
		
				totalTelevisiones = totalTelevisiones + arrayObjetos[i].precioFinal();
			}
			
			
			
			
		
		}
		
		System.out.println("El total de televiones es " + totalTelevisiones); // And now we print all of the results
		System.out.println(" El total de lavadoras es " + totalLavadoras);
		System.out.println(" Y el general de electrodomesticos es " + totalElectrodomesticos);
		
	}

}
