import model.cine;
import model.espectador;
import model.pelicula;

public class CineApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Creamos el objeto pelicula con los datos
		pelicula pelicula1 = new pelicula("Doctor DoLittle", 160, 7, "Tarantino");
		
		// Creamos el objeto cine con el precio de la entrada y el objeto pelicula por parametros
		cine cine1 = new cine(8, pelicula1);
		
		// Creamos el objeto espectadores en forma de array
		espectador espectadores[] = new espectador[100];
		
		// Creamos un array con los nombres de los espectadores
		String nombresEspectadores[] = {"Alberto", "Oscar", "Pau", "Daniel", "Sergi", "Aleix", "Marc"};
		
		// Creamos el array de las edades
		int edades[] = {10, 5, 18, 20, 25, 45, 30, 3};
		
		// Creamos el array del dinero de los espectadores
		int dineroEspectadores[] = {20, 5, 4, 3, 15, 12, 25};
		
		// Avisamos a los usuarios que pelicula se va a emitir
		System.out.println("La pel�cula que se va a emitir hoy es : " + pelicula1.toString().toUpperCase());
		System.out.println("");
		
		// Mostramos la capacidad de la sala
		System.out.println("La capacidad de nuestra sala es = " + (cine1.getNumfilas() * cine1.getNumcolumnas()) + " personas");
		System.out.println("");
		
		// Creamos un contador para tener el dato de los asientos asignados
		int contAsientosAsignados = 0;
		
		// Creamos un for para recorrer la sala y empezar a asignar espectadores
		for (int i = 0; i < espectadores.length; i++) {
			
			// Asignamos un espectador al primer asiento libre con nombre, edad y dinero aleatorios
			espectadores[i] = new espectador(nombresEspectadores[(int) (Math.random() * nombresEspectadores.length)], 
					edades[(int) (Math.random() * edades.length)], dineroEspectadores[(int) (Math.random() * dineroEspectadores.length)]);
		
			// Avisamos al espectador
			System.out.println("Espectador n�mero " + (i + 1) + " = " + espectadores[i].toString());
		
			// Que si cumple estos requisitos se le ha asignado un asiento
			if (espectadores[i].getDinero() >= cine1.getPrecioEntrada() && espectadores[i].getEdad() >= pelicula1.getEdadMinima() && !cine1.cineLleno()){
				cine1.darAsiento();
				System.out.println("Se le ha dado un asiento");
				System.out.println("");
				// Sumo 1 al contador de los asientos asignados
				contAsientosAsignados++;
			}
			// Si la sala esta llena muestra esto
			else if (cine1.cineLleno()) {
				System.out.println("Lo sentimos el cine est� a su m�xima capacidad");
				System.out.println("");
			}
			// Si no llega a la edad m�nima muestra esto
			else if (espectadores[i].getEdad() < pelicula1.getEdadMinima()) {
				System.out.println("No tienes la edad m�nima para entrar a ver la pel�cula");
				System.out.println("");
			}
			// Si no tiene suficiente dinero muestra esto
			else if (espectadores[i].getDinero() < cine1.getPrecioEntrada()) {
				System.out.println("No tienes suficiente dinero para entrar a ver la pel�cula");
				System.out.println("");
			}
			
		}
		
		// Al final de la ejecuci�n mostramos la cantidad de plazas libres que han quedado en la sala
		if (!cine1.cineLleno()) {
			System.out.println("Se han asignado " + contAsientosAsignados + " asientos");
			System.out.println("A�n quedan " + ((cine1.getNumfilas() * cine1.getNumcolumnas()) - contAsientosAsignados) + " asientos");
		}
		// O si la sala ha quedado completamente llena
		else {
			System.out.println("Las plazas del cine est�n todas ocupadas");
		}
		
	}

}
