package dto;

public class Television extends Electrodomestico {
	private final static int  PULGADASdefault = 20; // We create the default values
	private final static boolean SINTONIZADORTDTdefault = false;
	
	
	
	private int pulgadas;
							// And the non-default attributes
	private boolean sintonizadorTDT;
	
	
	public Television() { // We create the default constructor
		this(PRECIOBASEdefault,PESOdefault,CONSENERGETICOdefault,COLORdefault,PULGADASdefault,SINTONIZADORTDTdefault);
		
	}
	
	public Television(double precioBase,double peso) { // And the one with only two parametres
		this(precioBase,peso,CONSENERGETICOdefault,COLORdefault,PULGADASdefault,SINTONIZADORTDTdefault);
	}
	
	
	  public Television(double precioBase, double peso, char consumoEnergetico, String color, int pulgadas, boolean sintonizadorTDT) {
		  super(precioBase,peso,consumoEnergetico,color);	// Now we create one with all the attributes
		  this.pulgadas = pulgadas;
		  this.sintonizadorTDT = sintonizadorTDT;
	  }
	
	



	
	
	
	public int getPulgadas() {
		return pulgadas; // We create the get methods 
	}

	

	public boolean isSintonizadorTDT() {
		return sintonizadorTDT;
	}


	public  double precioFinal() { // We override the 'precioFinal' method again, because we have to 
								   // add 'pulgadas' and 'sinonizador' to the method.
		
		double bonus = super.precioFinal();
		if(pulgadas > 40) { // If it is bigger than 40
			
			bonus += (precioBase / 100) * 30; // We add a 30% more to the final price
			
			
		}
		
		if (sintonizadorTDT == true) { // If it has sintonizador
			bonus = bonus + 50; // We add 50 euros to the final result
		}
		
		
		
		return bonus;
		
	}
	
}

	
	


