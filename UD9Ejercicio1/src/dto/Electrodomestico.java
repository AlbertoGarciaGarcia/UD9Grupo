package dto;

public class Electrodomestico {
// We create constants variables because they are all the same to all of the objects
	
	protected  final static  double PRECIOBASEdefault = 5;
	protected final static String COLORdefault = "blanco";
	protected final static char CONSENERGETICOdefault = 'F';	// We make all the constants 'final' because they are the default values
	protected final static double PESOdefault = 5;
	
	
	protected double precioBase; // We make the values protected because this class is the father one.
	
	protected String color; 
	
	protected char consumoEnergetico;
	
	
	protected double peso;
	
	
	public Electrodomestico() { // We establish the default constructor to assign the default values  to the ones that we have created
		this(PRECIOBASEdefault,PESOdefault,CONSENERGETICOdefault,COLORdefault);
	}

	public Electrodomestico(double precioBase, double peso){ // And now we create one with just two values given by the app
        this(precioBase, peso, CONSENERGETICOdefault, COLORdefault);
    }
	
	 public Electrodomestico(double precioBase, double peso, char consumoEnergetico, String color){ // And finally a constructor with al of the paraemeters
		 this.precioBase=precioBase;																// With an exception of the 'consumo' and with the color', because we have to use
		 this.peso=peso;																			// two methods to make sure they are valid ones  
	     comprobarConsumo(consumoEnergetico); // We use the methods to check if the values are valids
	     comprobarColor(color);
	     
	    }
	
	
	
	
	// We create the getters for all the values
	public double getPrecioBase() {
		return precioBase;
	}


	public String getColor() {
		return color;
	}


	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	
	

	public double getPeso() {
		return peso;
	}



	// We make a method to make sure the value of the color it's a valid one, otherwise we will make it the default one
	private  String comprobarColor(String color ) {
		
		if(color.equalsIgnoreCase("Blanco")||color.equalsIgnoreCase("Rojo")||color.equalsIgnoreCase("Negro")||color.equalsIgnoreCase("Azul")||color.equalsIgnoreCase("Gris") ) {
			return this.color = color;
		}	// We return the color because it's valid
		else {
			return this.color="blanco"; // We return the default one because it's not a valid color
		}
	}
	
	
	private char comprobarConsumo(char consumo) {
        if (consumo == 'A' || consumo == 'B' ||  consumo == 'C' || consumo == 'D' || consumo == 'F') {
            return this.consumoEnergetico = consumo;
        }else {// If the energetic consumption does not equal the valid ones we assign it to the default one 
            return this.consumoEnergetico ='F';
        }
    }
	// Now we create the method precioFinal to calculate the final price
	public  double precioFinal() {
		// We create the variables to add the bonus price to the final one 
		double precioFinal = 0;
		int bonusLetra = 0;
		int bonusPeso = 0;
		
		if(consumoEnergetico == 'A') // Depending on the character, we will add a value or another
			bonusLetra = 100;
		else if(consumoEnergetico == 'B')
			bonusLetra = 80;
		else if(consumoEnergetico == 'C') {
			bonusLetra = 60;
		}
		else if(consumoEnergetico == 'D') {
			bonusLetra = 50;
		}
		else if(consumoEnergetico == 'E') {
			bonusLetra = 30;
		}
		else if(consumoEnergetico == 'F')
			bonusLetra = 10;
		// We'll do the same with the weight 
		if(peso > 0 && peso < 19 ) {
			bonusPeso = 10;
		}
		else if(peso > 20 && peso < 49 ) {
			bonusPeso = 50;
		}
		else if(peso > 50 && peso < 79 ) {
			bonusPeso = 80;
		}
		else if(peso > 80) {
			bonusPeso = 100;
		}
		// And finally we will add both of the bonus prices to the final one and we will return it
		precioFinal = precioBase + bonusLetra + bonusPeso;
		
		
		return precioFinal;
	}


	
	
	
	
}

