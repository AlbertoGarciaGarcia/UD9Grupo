package model;

import util.Entregable;

public class videojuego implements Entregable{

	// Constantes
	protected final static int horasDefecto = 10;
	
	protected final static boolean entrega = false;
	
	// Atributos
	protected String titulo;
	
	protected int horasEstimadas;
	
	protected boolean entregado;
	
	protected String genero;
	
	protected String compa�ia;

	// Constructores
	public videojuego() {
		this.titulo = "";
		this.horasEstimadas = horasDefecto;
		this.entregado = entrega;
		this.genero = "";
		this.compa�ia = "";
	}

	public videojuego(String titulo, int horasEstimadas) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = entrega;
		this.genero = "";
		this.compa�ia = "";
	}

	public videojuego(String titulo, int horasEstimadas, String genero, String compa�ia) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.genero = genero;
		this.compa�ia = compa�ia;
		this.entregado = entrega;
	}

	// Getters y setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	// ToString
	@Override
	public String toString() {
		return "videojuego [El t�tulo del juego es:" + titulo + ", las horas estimadas de juego son:" + horasEstimadas + ", ha sido entregada?" + entregado
				+ ", el g�nero de este videojuego es:" + genero + ", y la compa�ia creador es:" + compa�ia + "]";
	}

	// M�todos implementados
	@Override
	public void entregar() {
		entregado = true;
		
	}

	@Override
	public void devolver() {
		entregado = false;
		
	}

	@Override
	public boolean isEntregado() {
		if(entregado == true) {
			return true;
		}
		else {
			return false;
		}
		
	}

	// Creo el compareTo
	@Override
	public String compareTo(Object objeto) {
		
		// Defino la cantidad de horas estimadas a peque�o
		String cantHorasEstimadas = "Menor";
		
		// Creo el objetoVideojuego el cual ser� igual al objeto pasado por par�metro casteado a videojuego
		videojuego objetoVideojuego = (videojuego) objeto;
		
		// Comparamos el n�mero de horas estimadas y definimos la cantidad de temporadas dependiendo al resultado
		if(horasEstimadas > objetoVideojuego.getHorasEstimadas()) {
			cantHorasEstimadas = "Mayor";
		}
		else if(horasEstimadas == objetoVideojuego.getHorasEstimadas()) {
			cantHorasEstimadas = "Igual";
		}
		
		return cantHorasEstimadas;
	}
	
	
	
	
	
	
}
