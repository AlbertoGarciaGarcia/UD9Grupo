package dto;

public class Lavadora extends Electrodomestico{

	private final static int CARGAdefault = 5; // We establish the default value to the 'Carga'
	
	
	private int carga; // We create the new attribute 'carga'
	
	
	public Lavadora() { // We create the default constructor with the default values, like the 'Electrodomestico' one
		this(PRECIOBASEdefault,PESOdefault,CONSENERGETICOdefault,COLORdefault,CARGAdefault);
		
	}
	
	 public Lavadora(double precioBase, double peso){ // We create the the constructor where we give it only two values
	        this(precioBase, peso, CONSENERGETICOdefault,COLORdefault,CARGAdefault);
	    }
	
	public Lavadora(double precioBase, double peso, char consumoEnergetico, String color, int carga){ // And another constructor with all the values
        super(precioBase,peso, consumoEnergetico,color);
        this.carga=carga;
    }

	
	
	
	
	
	
	
	

	
	
	
	
	public int getCarga() { // We use a getter for the attribute 'carga', because we already used getter methods for the rest of the attributes.
		return carga;
	}

	
	
	
	
	
	
	
	


public  double precioFinal() { // We override de precio final method, because it will be slightly different in this case
	double precioBonus = super.precioFinal();
	
	if(carga > 30) { // If the carga is higher than 30
		precioBonus = precioBonus + 50; // We add 50 euros to the final price
	

	}
	
	return precioBonus;
}






}